# Steps to run Rails on Vagrant

**Requirements**
 -  [Virtual Box](https://www.virtualbox.org/)
 -  [Vagrant](https://www.vagrantup.com/)

Go to Project directory and load vagrant up and run provision on Vagrant

> Initial `vagrant up` will always run with provision.

```bash  
vagrant up
```  

SSH into Vagrant
```bash  
vagrant ssh
```  
On Vagrant ssh, go to project directory and install bundle and yarn.

> Project Directory can be changed in *Vagrantfile*

```bash  
cd /home/project
bunde install
yarn install
```  
After successful install, run Rails DB command to setup DB.
```bash  
rails db:create # To create DB
rails db:migrate # To create tables on DB
rails db:seed # To generate dummy data on tables
```
To rails server start

> On vagrant, rails (puma) server should bind to host **0.0.0.0** or else it cannot run from host browser.

```bash  
rails server -b 0.0.0.0
```

On Host Browser, run this address **[192.168.56.99:3000](http://192.168.56.99:3000)**. This IP address can also be changed on *Vagrantfile*.

-----
